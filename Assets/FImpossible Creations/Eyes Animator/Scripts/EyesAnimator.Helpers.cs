﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FIMSpace.FEyes
{
    public partial class FEyesAnimator : UnityEngine.EventSystems.IDropHandler, IFHierarchyIcon
    {
        public bool debugSwitch = false;
        public string EditorIconPath { get { return "Eyes Animator/EyesAnimator_IconSmall"; } }
        public void OnDrop(UnityEngine.EventSystems.PointerEventData data) { }


    }
}