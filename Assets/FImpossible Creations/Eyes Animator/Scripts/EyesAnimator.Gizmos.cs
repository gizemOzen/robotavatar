﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FIMSpace.FEyes
{
    public partial class FEyesAnimator
    {
        public bool _gizmosDrawMaxDist = true;

        public bool DrawGizmos = true;

        protected virtual void OnDrawGizmosSelected()
        {
            if (!DrawGizmos) return;

            if (EyesTarget != null && HeadReference != null)
            {
                Vector3 lookStartPositionBase;
                lookStartPositionBase = transform.position;
                lookStartPositionBase.y = HeadReference.position.y;
                lookStartPositionBase += HeadReference.TransformVector(StartLookOffset);

                Gizmos.color = new Color(0.3f, 1f, 0.3f, 0.7f);
                Gizmos.DrawLine(lookStartPositionBase, (EyesTarget.position + targetLookPositionOffset));
            }


            if (HeadReference != null)
            {
                ComputeReferences();
                Vector3 f;

                Gizmos.color = new Color(0.3f, 0.3f, 1f, 0.7f);
                for (int i = 0; i < Eyes.Count; i++)
                {
                    if (Eyes[i] == null) continue;
                    Quaternion eyeForwarded = Eyes[i].rotation * Quaternion.Inverse(Quaternion.FromToRotation(eyeForwards[i], Vector3.forward));
                    f = eyeForwarded * Vector3.forward;

                    Gizmos.DrawRay(Eyes[i].position, f);
                    Gizmos.DrawLine(Eyes[i].position + f, Eyes[i].position + f * 0.6f + eyeForwarded * Vector3.right * 0.3f);
                    Gizmos.DrawLine(Eyes[i].position + f, Eyes[i].position + f * 0.6f + eyeForwarded * Vector3.left * 0.3f);
                }

                Vector3 middle = Vector3.Lerp(transform.position, HeadReference.position, 0.5f);
                Gizmos.DrawSphere(middle, 0.1f);
                Quaternion headForwarded = HeadReference.rotation * Quaternion.FromToRotation(headForward, Vector3.forward);
                f = headForwarded * Vector3.forward;

                Gizmos.DrawRay(middle, f);
                Gizmos.DrawLine(middle + f, middle + f * 0.65f + headForwarded * Vector3.right * 0.3f);
                Gizmos.DrawLine(middle + f, middle + f * 0.65f + headForwarded * Vector3.left * 0.3f);
            }

            Gizmos_DrawMaxDistance();
        }


        private void Gizmos_DrawMaxDistance()
        {
            if (EyesMaxDistance <= 0f) return;
            if (!_gizmosDrawMaxDist) return;

            Vector3 startLook = GetStartLookPosition();
            float a = 0.525f;
            Gizmos.color = new Color(.1f, .835f, .08f, a);

            Gizmos.DrawWireSphere(GetStartLookPosition(), EyesMaxDistance);

            if (MaxOutDistanceFactor > 0f)
            {
                Gizmos.color = new Color(.835f, .135f, .08f, a);
                Gizmos.DrawWireSphere(GetStartLookPosition(), EyesMaxDistance + EyesMaxDistance * MaxOutDistanceFactor);

            }

            Gizmos.color = new Color(0.02f, .65f, 0.2f, a);
            Gizmos.DrawLine(startLook - Vector3.right * (EyesMaxDistance + EyesMaxDistance * MaxOutDistanceFactor), startLook + Vector3.right * (EyesMaxDistance + EyesMaxDistance * MaxOutDistanceFactor));
            Gizmos.DrawLine(startLook - transform.forward.normalized * (EyesMaxDistance + EyesMaxDistance * MaxOutDistanceFactor), startLook + Vector3.forward * (EyesMaxDistance + EyesMaxDistance * MaxOutDistanceFactor));
        }
    }
}