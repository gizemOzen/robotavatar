﻿#if EYES_LOOKANIMATOR_IMPORTED
using FIMSpace.FLook;
using UnityEngine;

namespace FIMSpace.FEyes
{
    public partial class FEyesAnimator
    {
        // If you are using also look animator, you can simply uncomment this and one LateUpdate() line for this feature
        public FLookAnimator LookAnimator = null;

        [Tooltip("Syncing object to follow")]
        public bool SyncTarget = true;
        [Tooltip("Syncing Clamping Ranges but in most cases eyes can use bigger ranges so please check it")]
        public bool SyncClamping = false;
        [Tooltip("Syncing 'Stop Look Above' and max distance ranges")]
        public bool SyncRanges = true;

        private void StartLookAnim()
        {
            // Queueing for component to be executed after look animator
            if (LookAnimator)
            {
                LookAnimator.enabled = false;
                LookAnimator.enabled = true;
            }

            enabled = false;
            enabled = true;
        }


        public void UpdateLookAnim()
        {
            if (LookAnimator == null) LookAnimator = GetComponentInChildren<FLookAnimator>();

            if (!LookAnimator) return;

            if (SyncClamping)
            {
                EyesClampHorizontal = LookAnimator.XRotationLimits;
                EyesClampVertical = LookAnimator.YRotationLimits;
            }

            if (SyncRanges)
            {
                EyesMaxDistance = LookAnimator.MaximumDistance;
                StopLookAbove = LookAnimator.StopLookingAbove;
            }

            SyncTheTarget();
        }


        public void SyncTheTarget()
        {
            if (SyncTarget)
            {
                if (LookAnimator != null)
                {
                    Transform target = LookAnimator.GetEyesTarget();
                    if (target != null) EyesTarget = target;
                }
            }
        }

    }
}
#endif
