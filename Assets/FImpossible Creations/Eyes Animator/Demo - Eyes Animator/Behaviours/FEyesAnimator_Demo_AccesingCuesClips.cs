﻿using FIMSpace.FEyes;
using UnityEngine;
using UnityEngine.UI;

public class FEyesAnimator_Demo_AccesingCuesClips : MonoBehaviour
{
    public AudioClip ImagVisual;
    public AudioClip ImagSound;
    public AudioClip Feeling;
    public AudioClip MemoVisual;
    public AudioClip MemoSound;
    public AudioClip SelfTalk;
    public AudioClip[] Think;
    public AudioClip SelfSing;
}
